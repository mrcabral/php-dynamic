<?php
define("TITLE", "Time | Janta das Aranhas");
include('includes/header.php');
?>

<div id="team-members" class="cf">
    <h1>Nosso time Aranha</h1>
    <p>
        Nosso time é pequeno peludo, e gostam de deixar os pratos bem enrolados por vários dias em um processo de maturação bem específico.
    </p>
    <hr>

    <?php
        foreach($teamMembers as $member){
            ?>
         <div class="member">
             <img src="assets/img/<?php echo $member['img']; ?>.jpg" alt="<?php echo $member['nome']?>">
             <h2><?php echo $member['nome']; ?></h2>
             <p><?php echo $member['bio']?></p>
         </div>
    <?php
        }
    ?>
</div>

<?php
include('includes/footer.php');
?>
