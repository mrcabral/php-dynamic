<?php

    define("TITLE", "Arrays");

    $numero = 6;
    $meu_nome = "Marco Cabral";

	$finim = array(
        array(
            "nome" => "Finim",
            "fator_medo" => "Alto",
            "media_crescimento_dias" => 14
        ),
        array(
            "nome" => "Salvador Dali",
            "fator_medo" => "Médio",
            "media_crescimento_dias" => 180
        ),
        array(
            "nome" => "Affonso Solano",
            "fator_medo" => "Baixo",
            "media_crescimento_dias" => 365
        ),
    );

?>

<!DOCTYPE html>
<html lang="pt">
	<head>
		<title>PHP <?php echo TITLE;?></title>
		<link href="assets/styles.css" rel="stylesheet">
	</head>
	<body>
		<div class="wrapper">
			<a href="/" title="Back to directory" id="logo">
				<img src="assets/img/logo.png" alt="PHP">
			</a>

			<h1>Tutorial <?php echo $numero?>: <small><?php echo TITLE;?></small></h1>
			<hr>

			<h2>Your Example</h2>

			<div class="sandbox">

				<h2>O Bigode <?php echo $finim[0]['nome'];?>!</h2>
				<p>Esse bigode parece mais um esquilo sujo! Ele tem um fator de maluquice nivel
                    <strong><?php echo $finim[0]['fator_medo'];?></strong> e leva
                    <strong><?php echo $finim[0]['media_crescimento_dias'];?> dias</strong> para crescer em média.
                </p>

                <h2>O Bigode <?php echo $finim[1]['nome'];?>!</h2>
                <p>Esse bigode parece mais um esquilo sujo! Ele tem um fator de maluquice nivel
                    <strong><?php echo $finim[1]['fator_medo'];?></strong> e leva
                    <strong><?php echo $finim[1]['media_crescimento_dias'];?> dias</strong> para crescer em média.
                </p>

                <h2>O Bigode <?php echo $finim[2]['nome'];?>!</h2>
                <p>Esse bigode parece mais um esquilo sujo! Ele tem um fator de maluquice nivel
                    <strong><?php echo $finim[2]['fator_medo'];?></strong> e leva
                    <strong><?php echo $finim[2]['media_crescimento_dias'];?> dias</strong> para crescer em média.
                </p>

            </div><!-- end sandbox -->

			<a href="index.php" class="button">Back to the lecture</a>

			<hr>

			<small>&copy;<!-- YEAR --> - <!-- YOUR NAME --></small>
		</div><!-- end wrapper -->

		<div class="copyright-info">
			<?php include('assets/includes/copyright.php'); ?>
		</div><!-- end copyright-info -->
	</body>
</html>
