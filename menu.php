<?php
define("TITLE", "Menu | Janta das Aranhas");
include('includes/header.php');
?>

<div id="menu-items">
    <h1>Nosso Delicioso Menu</h1>
    <p>
        Nossos pratos contém produtos orginais e de qualidades não encontrada em nenhum outro restaurante.
    </p>
    <p><em>Clique em um item do menu para saber mais: </em></p>
    <hr>

    <ul>

        <?php
        foreach($menuItems as $dish => $item){
            ?>
            <li>
                <a href="prato.php?item=<?php echo $dish; ?>"><?php echo $item['titulo']?></a>&nbsp;<sup>R$</sup><?php echo $item['preco']?>
            </li>
            <?php
        }
        ?>

    </ul>
</div>

<?php
include('includes/footer.php');
?>
