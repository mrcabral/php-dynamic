<?php
$companyName = "Janta das Aranhas";
include('includes/arrays.php');
?>

<!DOCTYPE html>
<html lang="pt">
<head>
    <title><?php echo TITLE?></title>
    <link href="assets/styles.css" rel="stylesheet">
</head>
<body id="final-example">
<div class="wrapper">
    <div id="banner">
        <a href="/" title="Retorne a pagina inicial">
            <img alt="banner-ruim" src="assets/img/banner.png">
        </a>
    </div>

    <div id="nav">
        <?php include('includes/nav.php'); ?>
    </div>

    <div class="content">
