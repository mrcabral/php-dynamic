<?php
	
	define("TITLE", "Arrays");

	$numero = 6;
	$meu_nome = "Marco Cabral";

    $bigodes = array("Finim", "Salvador Dali", "Affonso Solano")
	
?>

<!DOCTYPE html>
<html lang="pt">
	<head>
		<title><?php echo TITLE;?></title>
		<link href="assets/styles.css" rel="stylesheet">
	</head>
	<body>
		<div class="wrapper">
			<a href="/" title="Back to directory" id="logo">
				<img src="assets/img/logo.png" alt="PHP">
			</a>
			
			<h1>Lecture <?php echo $numero?>: <small><?php echo TITLE;?></small></h1>
			<hr>
			
			<h2>Your Example</h2>
			
			<div class="sandbox">
			
				<h2>Moustache Types</h2>
				<ul>
					<li><?php echo $bigodes[0];?></li>
                    <li><?php echo $bigodes[1];?></li>
                    <li><?php echo $bigodes[2];?></li>
				</ul>
				
			</div><!-- end sandbox -->
			
			<a href="index.php" class="button">Back to the lecture</a>
			
			<hr>
			
			<small>&copy;<!-- YEAR --> - <!-- YOUR NAME --></small>
		</div><!-- end wrapper -->
		
		<div class="copyright-info">
			<?php include('assets/includes/copyright.php'); ?>
		</div><!-- end copyright-info -->
	</body>
</html>
