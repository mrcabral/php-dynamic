<?php

$navItens = array(
    array(
        'slug' => 'home.php',
        'title' => 'Home'
    ),
    array(
        'slug' => 'time.php',
        'title' => 'Time'
    ),
    array(
        'slug' => 'menu.php',
        'title' => 'Menu'
    ),
    array(
        'slug' => 'contato.php',
        'title' => 'Contato'
    )
);

$teamMembers = array(
    array(
        'nome' => 'Pegajoso',
        'posicao' => 'Dono',
        'bio' => 'Descendo dos altos becos dos quartos ele empreendeu em terrenos nunca antes explorados por
         aventureiros de sua espécie',
        'img' => 'pegajoso'
    ),
    array(
        'nome' => 'Felpudo',
        'posicao' => 'Manager',
        'bio' => 'Braço direito do dono do negócio sempre compartilhavam suas glórias e derrotas, desde antes do
         restaurante',
        'img' => 'felpudo'
    ),
    array(
        'nome' => 'Octotávio',
        'posicao' => 'Chef',
        'bio' => 'Muito famoso em todo o mundo dos teioso por fazer as melhores gororobas',
        'img' => 'octotavio'
    ),
);

$menuItems = array(
    'sopa-de-mosca' => array(
        'titulo' => 'Sopa de Mosca',
        'preco' => '18,00',
        'descricao' => 'Melhor sopa do mercado',
        'bebida' => 'Sem bebida'
    ),
    'casca-de-louvadeus' => array(
        'titulo' => 'Casca de louva-a-deus',
        'preco' => '10,00',
        'descricao' => 'Snacks de casquinhas crocantes bem assadas',
        'bebida' => 'Suco'
    ),
    'enroladinho-de-formiga' => array(
        'titulo' => 'Enroladinho de Formiga',
        'preco' => '20,00',
        'descricao' => 'Especialidade da casa, vem enrolado em uma camada espeça do nosso igrediente especial',
        'bebida' => 'Soda'
    ),
);